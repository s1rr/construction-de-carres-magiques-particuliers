import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.animation

#####################
# Array preparation
#####################

#input array
n = 5
M = np.zeros((n, n))
#output array
res = np.zeros_like(M)

#####################
# Create inital plot
#####################
fig = plt.figure(figsize=(8,4))

def add_axes_inches(fig, rect):
    w,h = fig.get_size_inches()
    return fig.add_axes([rect[0]/w, rect[1]/h, rect[2]/w, rect[3]/h])

axwidth = 3.
cellsize = 0.4
ax_res = add_axes_inches(fig, [4.5*cellsize, 2*cellsize, 10*cellsize, 6*cellsize])

im_res = ax_res.imshow(res, vmin=0, vmax=n^2*2, cmap="Greens")
res_texts = []
for i in range(res.shape[0]):
    row = []
    for j in range(res.shape[1]):
        row.append(ax_res.text(j,i, "", fontsize=16, va="center", ha="center"))
    res_texts.append(row)

ax_res.tick_params(left=False, bottom=False, labelleft=False, labelbottom=False)
ax_res.yaxis.set_major_locator(mticker.IndexLocator(1,0))
ax_res.xaxis.set_major_locator(mticker.IndexLocator(1,0))
ax_res.grid(color="k")

###############
# Animation
###############
def init():
    for row in res_texts:
        for text in row:
            text.set_text("")

i, j = 0, 0

def animate(k):
    global i, j
    k = k[0]

    # Calculate result print("res:" + str(k))
    

    if k < n^2:
        res_texts[i][j].set_text(k)
        r = res.copy()
        r[i,j] = k
        res[i, j] = k
    
        M[0, 0] = 0
        if M[(i + 1) % n, (j + 2) % n] == 0 and (not (mod(i + 1, n) == 0 and mod(j + 2, n) == 0)):
            i, j = (i + 1) % n, (j + 2) % n
            M[i, j] = k
        else:
            i, j = i, (j - 1) % n
            M[i, j] = k
        im_res.set_array(r)

    

k = range(n^2 + 10)
ani = matplotlib.animation.FuncAnimation(fig, animate, init_func=init, frames=zip(k), interval=300)
ani.save("cavalier-final.mp4", writer="ffmpeg")
plt.show()
