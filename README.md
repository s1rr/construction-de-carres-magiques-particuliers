# Magic square construction

The implementation of the constructions is in sage (built on python). The animations have been implemented in python using matplotlib.animation.


Magic square creation using the cavalier method:
![](https://gitlab.com/s1rr/construction-de-carres-magiques-particuliers/-/raw/main/cavalier-anim.gif)

Magic square creation with an alternative method:
![](https://gitlab.com/s1rr/construction-de-carres-magiques-particuliers/-/raw/main/creation-carre-anim.gif)
