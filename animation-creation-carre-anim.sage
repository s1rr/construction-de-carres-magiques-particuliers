import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.animation

#####################
# Array preparation
#####################

#input array
n = 5
M = np.zeros((n, n))
#output array
res = np.zeros_like(M)

#####################
# Create inital plot
#####################
fig = plt.figure(figsize=(8,4))

def add_axes_inches(fig, rect):
    w,h = fig.get_size_inches()
    return fig.add_axes([rect[0]/w, rect[1]/h, rect[2]/w, rect[3]/h])

axwidth = 3.
cellsize = 0.4
ax_res = add_axes_inches(fig, [4.5*cellsize, 2*cellsize, 10*cellsize, 6*cellsize])

im_res = ax_res.imshow(res, vmin=0, vmax=n^2*2, cmap="Greens")
res_texts = []
for i in range(res.shape[0]):
    row = []
    for j in range(res.shape[1]):
        row.append(ax_res.text(j,i, "", fontsize=16, va="center", ha="center"))
    res_texts.append(row)

ax_res.tick_params(left=False, bottom=False, labelleft=False, labelbottom=False)
ax_res.yaxis.set_major_locator(mticker.IndexLocator(1,0))
ax_res.xaxis.set_major_locator(mticker.IndexLocator(1,0))
ax_res.grid(color="k")

###############
# Animation
###############

gen_interne = 2
sigma = (1, 2, 3, 4, 5)

for j in range(n):
    M[0, j] += sigma[j] - 1

def init():
    j = 0
    for row in res_texts:        
        for text in row:
            text.set_text("")
    for text in res_texts[0]:
        text.set_text(sigma[j] - 1)
        j = j + 1

def animate(ij):
    global k
    i,j=ij

    M[i + 1, j] += M[mod(i, n), mod(j + gen_interne, n)]
    k = int(M[mod(i, n), mod(j + gen_interne, n)])

    # Calculate result print("res:" + str(k))
    res_texts[i + 1][j].set_text(k)
    r = res.copy()
    r[i + 1,j] = k
    res[i + 1, j] = k
    
    # FOR THE COLORING
    im_res.set_array(r)


i,j = np.indices(res.shape)
ani = matplotlib.animation.FuncAnimation(fig, animate, init_func=init, frames=zip(i.flat, j.flat), interval=400)
ani.save("creation-carre-anim.mp4", writer="ffmpeg")
plt.show()
