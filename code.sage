# CONSTRUCTION DE CARRES MAGIQUES PARTICULIERS
# Janvier 2021

###################################################
# I - Verification des proprietes carres magiques #
###################################################

Exemple_1 = matrix([[0, 14, 3, 13],
                    [7, 9, 4, 10],
                    [12, 2, 15, 1],
                    [11, 5, 8, 6]])

def matrix_to_list(M):
    ''' On change de structure afin de pouvoir appliquer des algorithmes de trie'''
    res = []
    for i in range(M.nrows()):
        res = res + list(M[i])
    return res

def carre_eulerien_p(M):
    list = matrix_to_list(M)
    list.sort()
    for i in range(len(list)):
        if list[i] != i:
            return False
    return True

def carre_pandiagonal_p(M):
    lenM = M.nrows()
    # Liste des sommes des différentes rangées
    liste_sommes_ligne, liste_sommes_col = [0] * lenM, [0] * lenM
    liste_sommes_diag_droite, liste_sommes_anti_diag = [0] * lenM, [0] * lenM

    for i in range(lenM):
        for j in range(lenM):
            liste_sommes_ligne[i] += M[i][j]
            liste_sommes_col[i] += M[j][i]
            liste_sommes_diag_droite[j] += M[j][mod(j + i, lenM)]
            liste_sommes_anti_diag[j] += M[j][mod(lenM - 1 - j - i, lenM)]
    # print("Sommes sur les lignes: " + str(liste_sommes_ligne))
    # print("Sommes sur les colonnes: " + str(liste_sommes_col))
    # print("Sommes sur les diagonales droites: " + str(liste_sommes_diag_droite))
    # print("Sommes sur les diagonales inverses: " + str(liste_sommes_anti_diag))

    for i in range(lenM): # on compare toutes les valeurs avec la somme sur la premiere ligne
        if liste_sommes_ligne[0] != liste_sommes_ligne[i] or liste_sommes_ligne[0] != liste_sommes_col[i]:
            return False
        elif liste_sommes_ligne[0] != liste_sommes_diag_droite[i] or liste_sommes_ligne[0] != liste_sommes_anti_diag[i]:
            return False
        else:
            return True

print("\nCarre de l'exemple 1 eulerien?"); print(carre_eulerien_p(Exemple_1))
print("\nCarre de l'exemple 1 pandiagonal?"); print(carre_pandiagonal_p(Exemple_1))
# print(cavalier(5)) # interpréter la fonction cavalier d'abord
# print("\nM eulerien?"); print(carre_eulerien_p(cavalier(5)))
# print("\nM pandiagonal?"); print(carre_pandiagonal_p(cavalier(5)))


#################
# II - Cavalier #
#################

def cavalier(n):
    if n%2==0:
        print("Le n donné est pair")
        return False
    if n%3==0:
        print("Le n donné est multiple de 3")
        return False
    M=matrix(n)
    i,j=0,0
    M[0,0]=1 # Le but est d'éviter la case initiale; on l'initialise à 1, puis on remettra 0 à la fin
    for k in range (1,n^2):
        if M[(i+1)%n,(j+2)%n]==0:
            # On peut sinon ajouter ce test: and (not (mod(i + 1, n) == 0 and mod(j + 2, n) == 0))
            i,j=(i+1)%n,(j+2)%n
            M[i,j]=k
        else:
            i,j=i,(j-1)%n
            M[i,j]=k
    M[0,0]=0
    return M

def cavalier_bis(n):
    if n%2==0 or n%3==0:
        print("le n donné est pair ou multiple de 3")
        return False
    M=matrix(n)
    for i in range (n):
        for j in range (n):
            M[i,j]= (3*i-j)%n + n*((2*i-j)%n)
    return M

print('\nCavalier pour n = 7'); print(cavalier(7))
# print('\nCavalier pour n = 11'); print(cavalier(11)); print("\n")
# print(cavalier_bis(11))
# print("\nCarre de l'exemple 1 eulerien?"); print(carre_eulerien_p(cavalier(7)))
# print("\nCarre de l'exemple 1 pandiagonal?"); print(carre_pandiagonal_p(cavalier(7)))


########################
# III - Généralisation #
########################


def conversion_10_vers_base(M):
    lenM = M.nrows()
    for i in range(lenM):
        for j in range(lenM):
            M[i,j] = (M[i,j] // lenM) * 10 + M[i,j] % lenM
    return M

def conversion_base_vers_10(M):
    lenM = M.nrows()
    for i in range(lenM):
        for j in range(lenM):
            M[i,j] = (M[i,j] // 10) * lenM + M[i,j] % 10
    return M

Mtest = cavalier(5)

Mtestbase5 = matrix([[00, 44, 33, 22, 11],
                     [23, 12, 01, 40, 34],
                     [41, 30, 24, 13, 02],
                     [14, 03, 42, 31, 20],
                     [32, 21, 10, 04, 43]])

# print('\nConversion: '); print(conversion_10_vers_base(cavalier(5))); print("\n")
# print(conversion_base_vers_10(Mtestbase5))

def decomposition_carre_eulerien(M):
    '''Carré eulérien vers deux carrés latins'''
    lenM = M.nrows()
    res1 = zero_matrix(lenM)
    res2 = zero_matrix(lenM)
    for i in range(lenM):
        for j in range(lenM):
            res1[i, j] = M[i,j] // lenM
            res2[i, j] = M[i,j] % lenM
    return res1, res2

print('\nDecomposition: '); print(cavalier(5)) # on test de voir si l'on retombe bien sur les mêmes résultats que l'exemple
print(decomposition_carre_eulerien(cavalier(5)))

Latin1 = matrix([[0, 3, 2, 1, 4],
                         [2, 1, 4, 0, 3],
                         [4, 0, 3, 2, 1],
                         [3, 2, 1, 4, 0],
                         [1, 4, 0, 3, 2]])

Latin2 = matrix([[4, 1, 2, 0, 3],
                         [0, 3, 4, 1, 2],
                         [1, 2, 0, 3, 4],
                         [3, 4, 1, 2, 0],
                         [2, 0, 3, 4, 1]])

Latin3 = matrix([[0, 0, 0, 0, 0], # Pas latin, pour test
                         [1, 1, 1, 1, 1],
                         [2, 2, 2, 2, 2],
                         [3, 3, 3, 3, 3],
                         [4, 4, 4, 4, 4]])

def carre_latin_pandiagonal_p(M):
    lenM = M.nrows()
    # cst_magique = lenM * ((lenM - 1) / 2)
    liste_a_trouver = [i for i in range(lenM)]
    # Liste des sommes des différentes rangées
    liste_sommes_ligne = [[] for _ in range(lenM)]
    liste_sommes_col = [[] for _ in range(lenM)]
    liste_sommes_diag_droite = [[] for _ in range(lenM)]
    liste_sommes_anti_diag = [[] for _ in range(lenM)]

    for i in range(lenM):
        for j in range(lenM):
            liste_sommes_ligne[i].append(M[i][j])
            liste_sommes_col[i].append(M[j][i])
            liste_sommes_diag_droite[j].append(M[j][mod(j + i, lenM)])
            liste_sommes_anti_diag[j].append(M[j][mod(lenM - 1 - j - i, lenM)])

    # print("Sommes sur les lignes: " + str(liste_sommes_ligne)) # DEBUG
    # print("Sommes sur les colonnes: " + str(liste_sommes_col))
    # print("Sommes sur les diagonales droites: " + str(liste_sommes_diag_droite))
    # print("Sommes sur les diagonales inverses: " + str(liste_sommes_anti_diag))
    for i in range(lenM):
        liste_sommes_ligne[i].sort()
        liste_sommes_col[i].sort()
        liste_sommes_diag_droite[i].sort()
        liste_sommes_anti_diag[i].sort()
    # print("\n\nSommes sur les lignes: " + str(liste_sommes_ligne)) # DEBUG
    # print("Sommes sur les colonnes: " + str(liste_sommes_col))
    # print("Sommes sur les diagonales droites: " + str(liste_sommes_diag_droite))
    # print("Sommes sur les diagonales inverses: " + str(liste_sommes_anti_diag))

    for i in range(lenM): # on compare toutes les valeurs avec la liste a trouver, par ex pour n = 4; [0, 1, 2, 3]
        if liste_a_trouver != liste_sommes_ligne[i] or liste_a_trouver != liste_sommes_col[i]:
            return False
        elif liste_a_trouver != liste_sommes_diag_droite[i] or liste_a_trouver != liste_sommes_anti_diag[i]:
            return False
        else:
            return True

print("\nLatin1 carre Latin pandiagonal?"); print(carre_latin_pandiagonal_p(Latin1))
print("\nLatin2 carre Latin pandiagonal?"); print(carre_latin_pandiagonal_p(Latin2))
# print("\nLatin3 carre Latin pandiagonal?"); carre_latin_pandiagonal_p(Latin3)

def generateurs_internes_znz(n):
    list = []
    G = Zmod(n)
    H = G.list_of_elements_of_multiplicative_group()
    for i in H:
        if (i - 1) in H and (i + 1) in H:
            list.append(i)
    return list

# print(generateurs_internes_znz(25))

def liste_permutations_possibles(n):
    G = SymmetricGroup(n)
    return G

def liste_permutations_possibles_ordre_lexicographique(n):
    liste_permutations_possibles = []
    permu = [i for i in range(n)]
    permu_finale = [(n - i - 1) for i in range(n)]
    # permu[:] pour créer une copie, car si on utiliser directement permu, c'est une "shallow copy" et non "hard"
    liste_permutations_possibles.append(permu[:])
    while permu != permu_finale:
        q = 0
        for i in range(n - 1): # plus grand i tq sigma(i) < sigma(i+1)
            if permu[i + 1] > permu[i]:
                q = i
        r = q + 1
        for i in range(q, n): # plus petit element plus grand que sigma(i)
            if permu[q] < permu[i] and permu[i] < permu[r]:
                r = i
        (permu[q], permu[r]) = (permu[r], permu[q])
        sublist = permu[q+1:] # on trie la liste apres q + 1
        sublist.sort()
        permu = permu[:q+1] + sublist
        liste_permutations_possibles.append(permu[:])
    return liste_permutations_possibles, len(liste_permutations_possibles)

print("\nPermutation possible ordre lexicographique: ")
print(liste_permutations_possibles_ordre_lexicographique(3))

def carres_latins_possibles(n):
    G = SymmetricGroup(n)
    gens = generateurs_internes_znz(n)
    return G, gens

# print(carres_latins_possibles(11))

def propriete_carre_latin(M):
    '''Propriété relatif aux carrés latins produit selon la méthodes'''
    lenM = M.nrows()
    first_row = list(M[0]) # Problème de type, résolu en convertissant en liste
    gen_interne = generateurs_internes_znz(lenM)
    if gen_interne == []:
        return("Aucun générateur interne!")
    for i in range(1, lenM + 1): # on decale la premiere rangee jusqu'a tomber sur la seconde
        first_row = first_row[-1:] + first_row[:-1] # on concatène le dernier élément avec tous les éléments sauf ce dernier
        if first_row == list(M[1]):
            gen_interne = i
            break
    sigma = M[0]
    return gen_interne, sigma

print("\nPropriété: "); print(propriete_carre_latin(cavalier(5)))

def independance_carre_latin_p(carre_latin1, carre_latin2):
    n = carre_latin1.nrows()
    (gen_interne1, sigma1) = propriete_carre_latin(carre_latin1)
    (gen_interne2, sigma2) = propriete_carre_latin(carre_latin2)
    F = Zmod(n)
    H = F.list_of_elements_of_multiplicative_group()
    if mod(gen_interne2 - gen_interne1, n) in H:
        return True
    else:
        return False

print("\nIndépendance: "); print(independance_carre_latin_p(Latin1, Latin2))

def creation_carre_eulerien_a_partir_latin(carre_latin1, carre_latin2):
    lenMi = carre_latin1.nrows()
    if not(carre_latin_pandiagonal_p(carre_latin1)) or not(carre_latin_pandiagonal_p(carre_latin2)):
        print("Attention, un des carres choisis n'est pas latin")
    if not independance_carre_latin_p(carre_latin1, carre_latin2):
        print("Attention, les carres choisis ne sont pas independants.")
    res = zero_matrix(lenMi)
    for i in range(lenMi):
        for j in range(lenMi):
            res[i, j] = carre_latin1[i,j] * lenMi + carre_latin2[i,j]
    return res

res1, res2 = decomposition_carre_eulerien(cavalier(5))
print("\nCreation à partir latin: "); print(creation_carre_eulerien_a_partir_latin(res1, res2))
print(creation_carre_eulerien_a_partir_latin(res1, res2) == cavalier(5))

def creation_carre_latin_pandiagonaux(gen_interne, sigma, n):
    lenM = n
    M = zero_matrix(lenM)
    for j in range(lenM):
        M[0, j] += sigma[j] - 1
    for i in range(1, lenM):
        for j in range(lenM):
            M[i, j] += M[mod(i - 1, n), mod(j + gen_interne, n)]
    return M

# Latin4 = creation_carre_latin_pandiagonaux(2, (1, 2, 3, 4, 5), 5); print(Latin4)
# print("\nCreation à partir générateur et sigma: "); print(carre_latin_pandiagonal_p(Latin4))

import random
def creation_carre_eulerien_a_partir_latin_independants_aleatoire(n):
    list_gen = generateurs_internes_znz(n)
    if list_gen == []:
        return ("Il n'y a aucun generateur interne pour n = " + str(n))
    G = SymmetricGroup(n)
    independant = False
    while independant == False:
        # creation de carre latin aleatoire
        carre_latin1 = creation_carre_latin_pandiagonaux(random.choice(list_gen), Permutation(G.random_element()), n)
        carre_latin2 = creation_carre_latin_pandiagonaux(random.choice(list_gen), Permutation(G.random_element()), n)
        if independance_carre_latin_p(carre_latin1, carre_latin2):
            independant = True
    carre_eulerien = creation_carre_eulerien_a_partir_latin(carre_latin1, carre_latin2)
    return carre_latin1, carre_latin2, carre_eulerien

print("\nGénération de carré magique aléatoire, pour n = 7:")
(carre_latin1, carre_latin2, carre_eulerien) = creation_carre_eulerien_a_partir_latin_independants_aleatoire(7)
print([carre_latin1, carre_latin2, carre_eulerien])
print("\ncarre_eulerien eulerien?"); print(carre_eulerien_p(carre_eulerien))
print("\ncarre_eulerien pandiagonal?"); print(carre_pandiagonal_p(carre_eulerien))

# print("\nPour n = 11:")
# (carre_latin3, carre_latin4, carre_eulerien2) = creation_carre_eulerien_a_partir_latin_independants_aleatoire(11)
# print([carre_latin3, carre_latin4, carre_eulerien2])
# print("\ncarre_eulerien eulerien?"); print(carre_eulerien_p(carre_eulerien2))
# print("\ncarre_eulerien pandiagonal?"); print(carre_pandiagonal_p(carre_eulerien2))
